pushd android
# flutter build generates files in android/ for building the app
flutter build apk --no-sound-null-safety
./gradlew app:assembleAndroidTest
./gradlew app:assembleDebug -Ptarget=integration_test/app_test.dart
popd